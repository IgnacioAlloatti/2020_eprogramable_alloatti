var _respirador_8c =
[
    [ "ACTIVADO", "_respirador_8c.html#a615e98d0afec2149cd5f45224acdeeb6", null ],
    [ "BUFFER_SIZE", "_respirador_8c.html#a6b20d41d6252e9871430c242cb1a56e7", null ],
    [ "CERO_CORRIDO", "_respirador_8c.html#abceda6d3e4c4181f4635fc06f9cb5edd", null ],
    [ "DESACTIVADO", "_respirador_8c.html#a5b09ab92ce3c2eef34b06fad683e0a0c", null ],
    [ "Activar_Sistema", "_respirador_8c.html#a5ad8b442658d8fc14c3028ffdcd6b89e", null ],
    [ "Conversion_AD", "_respirador_8c.html#adf5c53061ad67810b445bf7fa844922d", null ],
    [ "main", "_respirador_8c.html#a840291bc02cba5474a4cb46a9b9566fe", null ],
    [ "Sist_Init", "_respirador_8c.html#a826cc949fbccf55218f669b7986fb347", null ],
    [ "Timer2_Interrupt", "_respirador_8c.html#a85d03dd1c1e6f8cdd46cb74c0609e694", null ],
    [ "Timer_Interrupt", "_respirador_8c.html#ad5a690e47edc2e2f9449cbd904e62668", null ],
    [ "analog_input", "_respirador_8c.html#a8800bcede073ac8036ca918474862155", null ],
    [ "aux", "_respirador_8c.html#a767676f52d238113b3f59412f19e411e", null ],
    [ "configuracion_uart", "_respirador_8c.html#a4001b2bc7d3f4256de2b8ed9fe218618", null ],
    [ "contador", "_respirador_8c.html#a90b81d8b975d17b99725ce3d676f0e4b", null ],
    [ "dato_leido", "_respirador_8c.html#a2feee18903467a8822b738f62617d4f9", null ],
    [ "dato_leido_anterior", "_respirador_8c.html#ac3cd96a7cd54605088aa2f8c6934e658", null ],
    [ "play", "_respirador_8c.html#a82af2642cfadb135cd3136277dab4eba", null ],
    [ "presion", "_respirador_8c.html#af78f2646d4f982d0a614df0589389800", null ],
    [ "termino", "_respirador_8c.html#a643eedbdbdcb697277733c5c8e105dd2", null ],
    [ "timer2", "_respirador_8c.html#adfab5bfbbe085c1428590c9917b88514", null ]
];