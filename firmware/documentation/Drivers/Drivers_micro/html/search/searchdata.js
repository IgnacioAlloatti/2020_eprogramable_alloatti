var indexSectionsWithContent =
{
  0: "_abcdefghilmnprstu",
  1: "adst",
  2: "abgstu",
  3: "agrstu",
  4: "bghimpst",
  5: "gi",
  6: "g",
  7: "dru",
  8: "abdgstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups"
};

var indexSectionLabels =
{
  0: "Todo",
  1: "Estructuras de Datos",
  2: "Archivos",
  3: "Funciones",
  4: "Variables",
  5: "Enumeraciones",
  6: "Valores de enumeraciones",
  7: "defines",
  8: "Grupos"
};

