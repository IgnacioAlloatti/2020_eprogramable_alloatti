var motor__pp_8c =
[
    [ "GirarMotor", "motor__pp_8c.html#afcb2ae3a894f02e308184c0bf2260cb8", null ],
    [ "Giro", "motor__pp_8c.html#afd29e51caaa28fffdbc57102a5762480", null ],
    [ "MotorDeinit", "motor__pp_8c.html#a2a43094e43d943728cdddd09b21f21ed", null ],
    [ "MotorInit", "motor__pp_8c.html#ae6641744c3bd72b7e5a14c26746a1ba1", null ],
    [ "MotorStart", "motor__pp_8c.html#a2cfb9f853a5c3e35d2cb93163c3c0e74", null ],
    [ "cont", "motor__pp_8c.html#a6fb6f14936678fce8ed305a84a52b675", null ],
    [ "GiroAntihorario", "motor__pp_8c.html#af06ea471b9534d8599acf8f4f0b7613f", null ],
    [ "GiroHorario", "motor__pp_8c.html#a77e3c52f8e7ad6590300c51121f84a57", null ],
    [ "i", "motor__pp_8c.html#af27e3188294c2df66d975b74a09c001d", null ],
    [ "inicio_timer", "motor__pp_8c.html#a8a1ca1e01cbf7f456bc4d19f6a6c72ba", null ],
    [ "motor", "motor__pp_8c.html#ad16594822d318864c1b80d998ed21cf5", null ],
    [ "Nciclos", "motor__pp_8c.html#a7045d4fcc1987347e9a2fc8e21d3ad5f", null ],
    [ "pin1", "motor__pp_8c.html#aed69219f65de52a602cd1966d59547cc", null ],
    [ "pin2", "motor__pp_8c.html#a12d0523d0bed2d73882cdf739782c621", null ],
    [ "pin3", "motor__pp_8c.html#ab26a01a58b039dce7dd46f035970077f", null ],
    [ "pin4", "motor__pp_8c.html#add0e84dec7b4829d2d618188c9a8eee7", null ],
    [ "sentido_giro", "motor__pp_8c.html#a51680c835cd8e6f34b3146ae8ac023d4", null ],
    [ "timer_i", "motor__pp_8c.html#a52cdbea22ddda49442d76184f33ce705", null ]
];