var indexSectionsWithContent =
{
  0: "_abcdefghilmnprstv",
  1: "m",
  2: "bdhlms",
  3: "dghlms",
  4: "acegimnpstv",
  5: "ls",
  6: "ls",
  7: "lmnprst",
  8: "bdls"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups"
};

var indexSectionLabels =
{
  0: "Todo",
  1: "Estructuras de Datos",
  2: "Archivos",
  3: "Funciones",
  4: "Variables",
  5: "Enumeraciones",
  6: "Valores de enumeraciones",
  7: "defines",
  8: "Grupos"
};

