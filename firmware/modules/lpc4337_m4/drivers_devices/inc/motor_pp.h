/*
 * motor_pp.h
 *
 *  Created on: 1 noviembre. 2020
 *      Author: Ignacio Alloatti
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_MOTOR_PP_H_
#define MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_MOTOR_PP_H_
/*==================[inclusions]=============================================*/

#include "bool.h"
#include "gpio.h"



/*==================[macros]=================================================*/
/*==================[typedef]================================================*/
typedef struct {/*!< Motor Struct*/
uint32_t numero_pasos_motor;/*!< Numero de pasos del motor a usar*/
uint16_t velocidad_giro;/*!< Velocidad de giro motor en grados por segundo*/
uint16_t angulo_giro;/*!< Angulo de giro motor*/

} motor_config;
/*==================[external data declaration]==============================*/
/*==================[external functions declaration]=========================*/
/** @brief Initialization function of motor_pp.h (motor paso a paso)
 *
 * Mapping ports (PinMux function), set direction .
 *
 * @param[in] gpio_t port pin1, gpio_t port pin2, gpio_t port pin3, gpio_t port pin4, motor_config struct
 *
 * @return TRUE if no error
 */
bool MotorInit(gpio_t pin_1, gpio_t pin_2, gpio_t pin_3, gpio_t pin_4,motor_config* configuracion);

/** @brief Funcion para poner en alto los pines correspondientes para cada paso
 *
 * pone en alto los pines necesarios para que el motor realice cada paso
 *
 * @param[in] gpio_on numero pin a poner en alto
 *
 *
 */
void Giro(uint8_t gpio_on);
/** @brief Funcion que se debe llamar para que el motor comience a girar
 *
 * pone a correr timer que genera la interrupcion para que los distintos pines se pongan en alto y gire el motor
 *
 * @param[in] sentido, 1=giro horario, 0=giro antihorario
 *
 * @return true cuando giro el angulo angulo_giro
 */
bool MotorStart(uint8_t sentido);
/** @brief Funcion para ordenar pasos del motor segun giro horario o antihorario
 *
 *  Llama a funcion giro y le pasa como parametro el pin que debe estar en alto para realizar el giro
 *
 */
void GirarMotor(void);
/** @brief Function to disable motor
 *
 *
 *
 */
bool MotorDeinit();

#endif
