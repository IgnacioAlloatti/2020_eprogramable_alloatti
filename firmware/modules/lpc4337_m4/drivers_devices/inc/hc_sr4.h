/*
 * hc_sr4.h
 *
 *  Created on: 2 sep. 2020
 *      Author: Ignacio Alloatti
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_HC_SR4_H_
#define MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_HC_SR4_H_
/*==================[inclusions]=============================================*/

#include "bool.h"
#include <stdint.h>
#include "gpio.h"


/*==================[macros]=================================================*/
/*==================[typedef]================================================*/
/*==================[external data declaration]==============================*/
/*==================[external functions declaration]=========================*/
/** @brief Initialization function of Hc_sr4.h (medidor de distancia ultrasonido)
 *
 * Mapping ports (PinMux function), set direction .
 *
 * @param[in] gpio_t port echo, gpio_t port trigger
 *
 * @return TRUE if no error
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);

/** @brief Function to read distance in cm
 *
 * supply a short 10uS pulse to the trigger, and read time echo signal high to calculate distance
 *
 * @param[in] no param
 *
 * @return distance in cm (time echo signal high/const)
 */
int16_t HcSr04ReadDistanceCentimeters(void);
/** @brief Function to read distance in In
 *
 * supply a short 10uS pulse to the trigger, and read time echo signal high to calculate distance
 *
 * @param[in] no param
 *
 * @return distance in In (time echo signal high/const)
 */
int16_t HcSr04ReadDistanceInches(void);
/** @brief Function to disable sensor
 *
 *
 * @param[in] gpio_t port echo, gpio_t port trigger
 *
 *
 */
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);

#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_HC_SR4_H_ */
