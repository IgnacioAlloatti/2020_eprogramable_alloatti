/*
 * hc_sr4.c
 *
 *  Created on: 2 sep. 2020
 *      Author: Ignacio Alloatti
 */
/** @brief Bare Metal driver for hc_sr4.
 **
 **/
/*==================[inclusions]=============================================*/
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
gpio_t echo_guardado , trigger_guardado;/**< Variables auxiliar para guardar pines de conexion */
uint16_t contador_tiempo;/**< Variable auxiliar para contar tiempo que echo esta en alto */
/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
/** @brief Function to init HcSr04Init */
bool HcSr04Init(gpio_t echo, gpio_t trigger)
{    echo_guardado=echo;
	 trigger_guardado=trigger;
	/** Configuration of the GPIO */
	GPIOInit(echo_guardado, GPIO_INPUT);
	GPIOInit(trigger_guardado, GPIO_OUTPUT);

	return true;

}
/** @brief Function to read distance in cm */
int16_t HcSr04ReadDistanceCentimeters(void)
{   contador_tiempo=0;
	GPIOOn(trigger_guardado);
	DelayUs(10);
	GPIOOff(trigger_guardado);
	while(GPIORead(echo_guardado)==0)
	{

	}
	while (GPIORead(echo_guardado)==1)
	{
		contador_tiempo = contador_tiempo + 1;
		DelayUs (1);
	}

	return contador_tiempo/23;

}
/** @brief Function to read distance in In*/
int16_t HcSr04ReadDistanceInches(void)
{   contador_tiempo=0;
	GPIOOn(trigger_guardado);
	DelayUs(10);
	GPIOOff(trigger_guardado);
	while(GPIORead(echo_guardado)==0)
	{
	}
	while (GPIORead(echo_guardado)==1)
	{
	contador_tiempo = contador_tiempo + 1;
	DelayUs (1);
	}

		return contador_tiempo/148;

}
/** @brief Function to disable sensor */
bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
{
	GPIODeinit();
	return true;
}





