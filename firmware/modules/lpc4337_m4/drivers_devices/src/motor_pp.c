/*
 * hc_sr4.c
 *
 *  Created on: 2 sep. 2020
 *      Author: Ignacio Alloatti
 */
/** @brief Bare Metal driver for hc_sr4.
 **
 **/
/*==================[inclusions]=============================================*/
#include "motor_pp.h"
#include "gpio.h"
#include "timer.h"
#include "systemclock.h"




/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
gpio_t pin1 , pin2, pin3,pin4 ;/**< Variables auxiliar para guardar pines de conexion */
motor_config motor;/**< Variables auxiliar para guardar configuracion de motor */
uint8_t GiroHorario [4]={1,2,3,4};/**< Variables auxiliar para guardar pasos para giro horario */
uint8_t GiroAntihorario [4]={1,4,3,2};/**< Variables auxiliar para guardar pasos para giro antihorario */
uint8_t sentido_giro;/**< Variables auxiliar para guardar sentido de giro */
uint32_t cont=0;/**< Variables auxiliar para contar numero de ciclos hasta que se cumpla el angulo */
uint8_t i=0;/**< Variables auxiliar para poner en alto el pin correspondiente*/
uint32_t Nciclos;/**< Variables auxiliar para guardar el numero de pasos a realizar para moverse el angulo necesario*/
uint8_t inicio_timer;/**< Variables auxiliar para guardar periodo timer*/
timer_config timer_i;/**< Variables auxiliar para guardar configuracion timer*/
/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
/** @brief funcion para iniciar motor paso a paso */
bool MotorInit(gpio_t pin_1, gpio_t pin_2, gpio_t pin_3, gpio_t pin_4,motor_config* configuracion)
{    pin1=pin_1;
     pin2=pin_2;
     pin3=pin_3;
     pin4=pin_4;
	/** Configuration of the GPIO */
	GPIOInit(pin1, GPIO_OUTPUT);
	GPIOInit(pin2, GPIO_OUTPUT);
	GPIOInit(pin3, GPIO_OUTPUT);
	GPIOInit(pin4, GPIO_OUTPUT);
	SystemClockInit();
	motor=*configuracion;
	Nciclos=motor.numero_pasos_motor*motor.angulo_giro/360;
	inicio_timer=(360*1000/motor.numero_pasos_motor)/motor.velocidad_giro;
	timer_config timer_init={TIMER_A,inicio_timer,&GirarMotor};
	timer_i=timer_init;
	TimerInit(&timer_i);
	return true;

}

/** @brief funcion para poner los distintos pines en alto*/
void Giro(uint8_t gpio_on)
{   switch (gpio_on){
	case 1:
		GPIOOff(pin4);
		GPIOOff(pin3);
		GPIOOff(pin2);
		GPIOOn(pin1);
		break;
	case 2:
		GPIOOff(pin4);
		GPIOOff(pin3);
		GPIOOn(pin2);
		GPIOOff(pin1);
		break;
	case 3:
		GPIOOff(pin1);
		GPIOOff(pin4);
		GPIOOff(pin2);
		GPIOOn(pin3);
		break;
	case 4:
		GPIOOff(pin1);
		GPIOOff(pin3);
		GPIOOff(pin2);
		GPIOOn(pin4);
		break;

					}

}
/** @brief funcion para iniciar giro del motor*/
bool MotorStart(uint8_t sentido)
{
	sentido_giro=sentido;

	TimerStart(TIMER_A);
	while(cont<Nciclos){

						}
	TimerStop(TIMER_A);
	cont=0;
	i=0;
return true;
}
/** @brief funcion para selleccionar giro del motor*/
void GirarMotor(void)
{
if (sentido_giro==1)
	{
	if (cont<Nciclos)
		{
		Giro(GiroHorario[i]);
		cont++;
		i++;
		}
	}
if (sentido_giro==0)
	{
	if (cont<Nciclos)
		{
		Giro(GiroAntihorario[i]);
		cont++;
		i++;
		}
	}

	if (i==4)
	{
		i=0;
	}



}
/** @brief Function to disable motor */
bool MotorDeinit()
{
	GPIODeinit();
	return true;
}





