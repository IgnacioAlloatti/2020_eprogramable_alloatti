/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * Ignacio Alloatti

 */
/** @mainpage Osciloscopio_Digital
*
* @section genDesc descripcion general
* Convierte señal analogica que entra por canal 3 en digital.
*
*
*
*
*
*
*@section changelog Cambios
*
* |   Date	   | Description                                    |
* |:----------:|:-----------------------------------------------|
* | 2/11/2020 | Creacion del documento	                        |
*
*
* @author Ignacio Alloatti
*
*/

/*==================[inclusions]=============================================*/
#include "../inc/Converso_AD.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "delay.h"
#include "analog_io.h"
#include "timer.h"
#include "uart.h"


/*==================[macros and definitions]=================================*/



/*==================[internal data definition]===============================*/

/** @def ACTIVADO
*	@brief Funcion del proyecto activada
*/
#define ACTIVADO 1
/** @def DESACTIVADO
*	@brief Funcion del proyecto desactivada
*/
#define DESACTIVADO 0

uint32_t datos_leidos[15];/**< Variable para guardar dato leido por ch3 */
uint8_t i=0;/**< Variable auxiliar para contador */
uint32_t suma=0;/**< Variable auxiliar para sumar los datos de un segundo */


/*==================[internal functions declaration]=========================*/
/** @brief Lee el dato que entra en el ch3 y lo envía por uart a pc.
 */
void Conversion_AD(void);
/** @brief Inicializacion de los distintos perifericos a usar
 */
void Sist_Init(void);
/** @brief Interrupcion cada 2 milisegundos para convertir dato analogico a digital.
 */
void Timer_Interrupt(void);



/*==================[external data definition]===============================*/

timer_config timer={TIMER_A ,67,Timer_Interrupt};/**< Configuracion del timer para convercion analogica digital */

analog_input_config analog_input={CH3,AINPUTS_SINGLE_READ,Conversion_AD};/**< Configuracion del conversor AD */
serial_config  configuracion_uart={SERIAL_PORT_PC,115200};/**< Configuracion de la uart */

/*==================[external functions definition]==========================*/
void Sist_Init(void)
{
	SystemClockInit();
	TimerInit(&timer);
	AnalogInputInit(&analog_input);
	UartInit(&configuracion_uart);

	TimerStart(TIMER_A);


}

void Timer_Interrupt(void)
{
	AnalogStartConvertion();
}
void Conversion_AD(void)
{
	AnalogInputRead(CH3, &datos_leidos[i]);
	i++;

	if (i==15){
		suma=0;
		for (uint8_t j=0;j<15;j++){
				suma=suma+datos_leidos[j];
			}
			uint32_t prom=suma/15;
			UartSendString(SERIAL_PORT_PC, UartItoa(prom, 10));
			UartSendString(SERIAL_PORT_PC,"\r\n");
				i=0;
			}

}




/** @fn int main(void)
*	@brief funcion principal.
*
*	@return 0
*/
int main(void)
{
    Sist_Init();

	while(1){

	}
	return 0;
}

/*==================[end of file]============================================*/

