/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * Ignacio Alloatti

 */
/** @mainpage Osciloscopio_Digital
*
* @section genDesc descripcion general
* Convierte señal analogica que entra por canal uno en digital. Para prueba se convierte señal digital de ecg en analogica, y se saca por el DAC.
*Permiteaplicar filtro digital pasa bajo.
*<a href="https://drive.google.com/file/d/1FVdGg_ZH6RVuc-30yuexiFhw1Nyk9utz/view?usp=sharing">Operation Example</a>
*
*
*
*
*@section changelog Cambios
*
* |   Date	   | Description                                    |
* |:----------:|:-----------------------------------------------|
* | 08/10/2020 | Creacion del documento	                        |
* |20/10/2020  |Se agrega filtro pasa bajo                      |
*
* @author Ignacio Alloatti
*
*/

/*==================[inclusions]=============================================*/
#include "../inc/Osciloscopio_Digital.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "delay.h"
#include "analog_io.h"
#include "timer.h"
#include "uart.h"


/*==================[macros and definitions]=================================*/



/*==================[internal data definition]===============================*/
/** @def BUFFER_SIZE
*	@brief tamaño de ecg de prueba
*/
#define BUFFER_SIZE 231


const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76
};/**< Vector de datos de ecg de prueba */
/** @def ACTIVADO
*	@brief Funcion del proyecto activada
*/
#define ACTIVADO 1
/** @def DESACTIVADO
*	@brief Funcion del proyecto desactivada
*/
#define DESACTIVADO 0
uint16_t filtro=DESACTIVADO;/**< Variable auxiliar para guardar estado filtro(activado/desactivado) */
uint16_t fc=100;/**< Variable auxiliar para guardar frecuencia de corte del filtro, comienza en 100 */
uint16_t dato_leido=0;/**< Variable para guardar dato leido por ch1 */
uint16_t dato_filtrado=0;/**< Variable para guardar dato leido por ch1 filtrado */
uint16_t dato_filtrado_anterior=0;/**< Variable para guardar dato filtrado anterior */
float alfa=0;/**< Variable auxiliar para calculo de dato filtrado */
uint8_t i=0;/**< Variable auxiliar para contador */


/*==================[internal functions declaration]=========================*/
/** @brief Lee el dato que entra en el ch1 y lo envía por uart a pc. si esta el filtro activado tambien envia el dato filtrado.
 */
void Conversion_AD(void);
/** @brief Inicializacion de los distintos perifericos a usar
 */
void Sist_Init(void);
/** @brief Interrupcion cada 2 milisegundos para convertir dato analogico a digital.
 */
void Timer_Interrupt(void);
/** @brief Interrupcion para sacar por dac el ecg de prueba.
 */
void Timer2_Interrupt(void);
/** @brief Activa el filtro por interrupcion de la tecla 1.
 */
void Activar_Filtro(void);
/** @brief Desactiva el filtro por interrupcion de la tecla 2.
 */
void Desactivar_Filtro(void);
/** @brief Baja fc de a 10 por interrupcion tecla 3.
 */
void BajarFrecCorte(void);
/** @brief Sube fc de a 10 por interrupcion tecla 4.
 */
void SubirFrecCorte(void);
/** @brief Filtra la señal leida en el ch1 con un filtro pasa bajo.
 */
void Filtrado(void);
/*==================[external data definition]===============================*/

timer_config timer={TIMER_A ,2,Timer_Interrupt};/**< Configuracion del timer para convercion analogica digital */
timer_config timer2={TIMER_B ,4,Timer2_Interrupt};/**< Configuracion del timer para salida por dac */
analog_input_config analog_input={CH1,AINPUTS_SINGLE_READ,Conversion_AD};/**< Configuracion del conversor AD */
serial_config  configuracion_uart={SERIAL_PORT_PC,115200};/**< Configuracion de la uart */

/*==================[external functions definition]==========================*/
void Sist_Init(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&timer);
	AnalogInputInit(&analog_input);
	UartInit(&configuracion_uart);
	AnalogOutputInit();
	TimerInit(&timer2);
	TimerStart(TIMER_A);
	TimerStart(TIMER_B);
	SwitchActivInt(SWITCH_1,&Activar_Filtro);
	SwitchActivInt(SWITCH_2,&Desactivar_Filtro);
	SwitchActivInt(SWITCH_3,&BajarFrecCorte);
	SwitchActivInt(SWITCH_4,&SubirFrecCorte);

}

void Timer_Interrupt(void)
{
	AnalogStartConvertion();
}
void Conversion_AD(void)
{
	AnalogInputRead(CH1, &dato_leido);
	if (filtro==ACTIVADO)
						{
						Filtrado();
						UartSendString(SERIAL_PORT_PC, UartItoa(dato_leido, 10));
						UartSendString(SERIAL_PORT_PC,",");
						UartSendString(SERIAL_PORT_PC, UartItoa(dato_filtrado, 10));
						UartSendString(SERIAL_PORT_PC,"\r");
						}
	else
		{
		UartSendString(SERIAL_PORT_PC, UartItoa(dato_leido, 10));
		UartSendString(SERIAL_PORT_PC,"\r");
		}

}
void Timer2_Interrupt(void)
{
	AnalogOutputWrite(ecg[i]);
	i++;
	if (i==230)
			{
			i=0;
			}
}
void Activar_Filtro(void)
{

	filtro=ACTIVADO;

}
void Desactivar_Filtro(void)
{

	filtro=DESACTIVADO;
}
void BajarFrecCorte(void)
{
	if (filtro==ACTIVADO&&fc>10)
	{
		fc=fc-10;
	}

}
void SubirFrecCorte(void)
{
	if (filtro==ACTIVADO&&fc<230)
	{
	fc=fc+10;
	}
}
void Filtrado(void)
{
	float RC=1/(2*3.14*fc);
	alfa=0.002/(0.002+RC);
	dato_filtrado=dato_filtrado_anterior+alfa*(dato_leido-dato_filtrado_anterior);
	dato_filtrado_anterior=dato_filtrado;

}
/** @fn int main(void)
*	@brief funcion principal.
*
*	@return 0
*/
int main(void)
{
    Sist_Init();

	while(1){

	}
	return 0;
}

/*==================[end of file]============================================*/

