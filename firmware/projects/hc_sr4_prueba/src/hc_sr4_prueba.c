/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * Ignacio Alloatti

 */
/** @mainpage Medidor de distancia led
*
* @section genDesc descripcion general
* Segun la distancia en que se encuentre el objeto se prendera una combinacion distinta de leds.
* Distancia entre 0 y 10 cm, led rgb azul.
* Distancia entre 10 y 20 cm, led rgb azul mas led 1
* Distancia entre 20 y 30 cm, led rgb azul mas led 1 mas led 2
* Distancia mayor a 30, todos los led
*
*<a href="https://drive.google.com/file/d/1Dcf5mkSh-w_4Wlvghh7ub1FGCY7i3yU-/view?usp=sharing">Operation Example</a>
*
*
* |   Device 1	   |   EDU-CIAA	    |
* |:--------------:|:--------------:|
* | 	Echo	   | 	GPIO_T_FIL2 |
* | 	Trigger	   | 	GPIO_T_FIL3	|
*
*@section changelog Cambios
*
* |   Date	   | Description                                    |
* |:----------:|:-----------------------------------------------|
* | 07/09/2020 | Creacion del documento	                        |
* |	18/09/2020 |Cambio secuencias de led porque se quemo el 2|
* @author Ignacio Alloatti
*
*/

/*==================[inclusions]=============================================*/
#include "../inc/hc_sr4_prueba.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "switch.h"
#include "delay.h"



/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
/** @def DISTANCIA_10
*	@brief distancia de 10 cm
*/
#define DISTANCIA_10 10
/** @def DISTANCIA_20
*	@brief distancia de 20 cm
*/
#define DISTANCIA_20 20
/** @def DISTANCIA_30
*	@brief distancia de 30 cm
*/
#define DISTANCIA_30 30
/** @def ACTIVADA
*	@brief Funcion del proyecto activada
*/
#define ACTIVADA 1
/** @def DESACTIVADA
*	@brief Funcion del proyecto desactivada
*/
#define DESACTIVADA 0

uint16_t result;/**< Variable auxiliar para guardar la distancia leida en cm */
uint16_t medicion=DESACTIVADA;/**< Variable auxiliar para guardar estado del sensor */
uint16_t hold=DESACTIVADA;/**< Variable auxiliar para guardar estado de funcion "mantener medicion" */


/*==================[internal functions declaration]=========================*/
/** @brief Activa/Desactiva la medicion del sensor Hc_sr4, por interrupcion al apretar la tecla 1
 */
void Activar_Medicion(void)
{
	medicion = !medicion;
}
/** @brief Activa/Desactiva la funcion "mantener medicion", por interrupcion al apretar la tecla 2
 */
void Hold(void)
{
	hold = !hold;
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
/** @fn int main(void)
*	@brief funcion principal.
*
*	Segun la lectura del sensor hc_sr4, se prenden distintas secuencias de led
*	@return 0
*/
int main(void)
{

	SystemClockInit();
    SwitchesInit();

	LedsInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	SwitchActivInt(SWITCH_1,&Activar_Medicion);
	SwitchActivInt(SWITCH_2,&Hold);
	while(1){

			 if(medicion==ACTIVADA)
			 	 	 	 	 	 	 {
				 	 	 	 	 	 result  = HcSr04ReadDistanceCentimeters();

				 	 	 	 	 	 if (result<=DISTANCIA_10)
				 	 	 	 	 	 {
				 	 	 	 	 	 LedOn(LED_RGB_B);
				 	 	 	 	 	 LedOff(LED_1);

									 LedOff(LED_3);

				 	 	 	 	 	 }
									 if (result<=DISTANCIA_20&&result>DISTANCIA_10)
									 {
							         LedOn(LED_RGB_B);
							         LedOn(LED_1);

									 LedOff(LED_3);

							         }
									 if (result<=DISTANCIA_30&&result>DISTANCIA_20)
									 {
									 LedOn(LED_RGB_B);

									 LedOn(LED_1);

								     LedOn(LED_3);

									 }
									 if (result>DISTANCIA_30)
									 {
									 LedOff(LED_RGB_B);
									 LedOn(LED_1);

									 LedOn(LED_3);
									 }
                                     while (hold==ACTIVADA)
                                     {
                                    	   if (medicion==DESACTIVADA)
                                    	   	   {
                                    		   hold=!hold;
                                    		   break;
                                    	   	   }
                                     }

			 	 	 	 	 	 	 }
			 if(medicion==DESACTIVADA)
			 	 	 	 {
				 	 	 LedOff(LED_RGB_B);
				 	 	 LedOff(LED_1);
				 	 	// LedOff(LED_2);
				 	 	 LedOff(LED_3);
				         }

			 DelayMs(500);
		 }

    
	return 0;
}

/*==================[end of file]============================================*/

