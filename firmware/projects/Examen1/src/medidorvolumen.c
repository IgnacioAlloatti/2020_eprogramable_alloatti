/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es:
 * Ignacio Alloatti

 */
/** @mainpage Medidor de volumen con envio de datos mediante UART-USB
*
* @section genDesc Descripcion general
* Mide la distancia desde arriba del vaso al liquido mediante sensor hc_sr4,calcula el volumen y envia datos para mostrar por UART-USB
*
*
*
*
* |   Device 1	   |   EDU-CIAA	    |
* |:--------------:|:--------------:|
* | 	Echo	   | 	GPIO_T_FIL2 |
* | 	Trigger	   | 	GPIO_T_FIL3	|
*
*@section changelog Cambios
*
* |   Date	   | Description                                    |
* |:----------:|:-----------------------------------------------|
* | 2/11/2020 | Creacion del documento	                        |
*
* @author Ignacio Alloatti
*
*/

/*==================[inclusions]=============================================*/
#include "../inc/medidorvolumen.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "switch.h"
#include "delay.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

/** @def ACTIVADA
*	@brief Funcion del proyecto activada
*/
#define ACTIVADA 1
/** @def DESACTIVADA
*	@brief Funcion del proyecto desactivada
*/
#define DESACTIVADA 0
/** @def RADIO
*	@brief Radio del vaso
*/
#define RADIO 3

uint16_t result;/**< Variable auxiliar para guardar la distancia leida en cm */

uint16_t volumen;/**< Variable auxiliar para guardar la parte entera del volumen medido  */

uint16_t volumen_decimales;/**< Variable auxiliar para guardar la parte decimales del volumen medido  */


/*==================[internal functions declaration]=========================*/




/** @brief Actualiza la medicion y la guarda en la variable result. Envia el dato por la UART
 */
void ActualizarMuestra(void);


/** @brief Inicializacion de los distintos perifericos a usar
 */
void Sistem_Inicialize(void);


/*==================[external data definition]===============================*/
timer_config timer_init={TIMER_A,333,ActualizarMuestra};/**< Configuracion del timer,cada 333ms mando dato calculo volumen y mando dato por uart para que se envien 3 datos por segundo */
serial_config configuracion_uart={SERIAL_PORT_PC,115200,NULL};/**< Configuracion de la UART */
/*==================[external functions definition]==========================*/

void ActualizarMuestra(void)
{
    result  = HcSr04ReadDistanceCentimeters();
    volumen=3*RADIO*RADIO*(10-result);
    volumen_decimales=14*RADIO*RADIO*(10-result);
    if (volumen_decimales<100){
    	UartSendString(SERIAL_PORT_PC,UartItoa(volumen, 10));
    	UartSendString(SERIAL_PORT_PC,".");
    	UartSendString(SERIAL_PORT_PC,UartItoa(volumen_decimales, 10));
    	UartSendString(SERIAL_PORT_PC,"cm3\r\n");

    }
    if (volumen_decimales==100||volumen_decimales>100){
    	volumen=volumen+volumen_decimales/100;
    	volumen_decimales=volumen_decimales-(volumen_decimales/100)*100;
     	UartSendString(SERIAL_PORT_PC,UartItoa(volumen, 10));
     	UartSendString(SERIAL_PORT_PC,".");
     	UartSendString(SERIAL_PORT_PC,UartItoa(volumen_decimales, 10));
     	UartSendString(SERIAL_PORT_PC,"cm3\r\n");

     }


}



void Sistem_Inicialize(void)
{	SystemClockInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	TimerInit(&timer_init);
	UartInit(&configuracion_uart);
	TimerStart(TIMER_A);
}




/** @fn int main(void)
*	@brief funcion principal.
*
*	@return 0
*/
int main(void)
{
	Sistem_Inicialize();

	while(1){


             }

    
	return 0;
}

/*==================[end of file]============================================*/

