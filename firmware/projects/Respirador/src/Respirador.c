/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * Ignacio Alloatti

 */
/** @mainpage Respirador
*
* @section genDesc descripcion general
* Lee la señal de presion y cuando esta pasa de positiva a negativa, se acciona el motor paso a paso para que
* aprete el ambu que ayudara al paciente a respirar.
*<a href="https://drive.google.com/file/d/1GwL5oSPU1Pr-5193RYw9CiOxR2RH-9mE/view?usp=sharing">Operation Example</a>
*
*
*
*
*@section changelog Cambios
*
* |   Date	   | Description                                    |
* |:----------:|:-----------------------------------------------|
* | 4/11/2020 | Creacion del documento	                        |
*
*
* @author Ignacio Alloatti
*
*/

/*==================[inclusions]=============================================*/
#include "../inc/Respirador.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "delay.h"
#include "analog_io.h"
#include "timer.h"
#include "uart.h"
#include "motor_pp.h"


/*==================[macros and definitions]=================================*/



/*==================[internal data definition]===============================*/
/** @def BUFFER_SIZE
*	@brief tamaño de ecg de prueba
*/
#define BUFFER_SIZE 299


const uint16_t presion[BUFFER_SIZE]={330,331,332,333,334,335,338,338,340,343,343,345,346,347,348,349,350,350,350,350,349,348,347,344,344,343,343,342,340,338,332,334,329,323,316,309,303,293,285,274,263,250,238,224,210,199,188,178,169,159,151,143,135,128,122,118,114,112,108,106,103,101,98,96,94,92,90,88,87,83,80,76,71,68,64,60,57,52,47,42,36,30,25,20,15,10,6,4,1,1,0,1,3,5,8,12,17,24,31,42,52,64,76,89,103,115,128,141,153,166,179,193,207,219,232,243,255,266,277,287,298,310,322,333,345,356,367,375,385,396,407,419,431,443,455,465,477,489,502,516,531,547,563,580,597,613,628,642,656,671,684,699,714,727,741,754,766,779,793,806,820,835,850,865,881,895,909,922,936,948,962,972,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,981,970,959,939,920,899,879,859,840,821,804,784,765,745,725,707,688,672,654,637,620,604,588,573,558,546,533,523,513,505,498,491,485,481,477,473,469,466,463,460,459,457,455,453,452,450,449,447,446,443,441,438,435,430,426,420,416,410,404,396,388,378,377,375,373,371,370,369,367,365,363,361,359,358,347,335,334,333,332,331,330};/**< Vector de datos de presion de prueba */

/** @def ACTIVADO
*	@brief Funcion del proyecto activada
*/
#define ACTIVADO 1
/** @def DESACTIVADO
*	@brief Funcion del proyecto desactivada
*/
#define DESACTIVADO 0
/** @def CERO_CORRIDO
*	@brief Valor que toma el cero al agregarle ofset a la señal
*/
#define CERO_CORRIDO 329
uint8_t play=ACTIVADO;/**< Variable auxiliar para activar o desactivar respirador(activado/desactivado) */
int32_t dato_leido=0;/**< Variable para guardar dato leido por ch1 */
int32_t dato_leido_anterior=0;/**< Variable para guardar dato leido anterior */
uint16_t contador=0;/**< Variable auxiliar para contador */
uint8_t aux=0;/**< Bandera para activar o desactivar el motor */
bool termino=true;/**< Variable para indicar si el motor termino de girar */
/*==================[internal functions declaration]=========================*/
/** @brief Lee el dato que entra en el ch1 y lo envía por uart a pc.
 */
void Conversion_AD(void);
/** @brief Inicializacion de los distintos perifericos a usar
 */
void Sist_Init(void);
/** @brief Interrupcion para convertir dato analogico a digital.
 */
void Timer_Interrupt(void);
/** @brief Interrupcion para sacar por dac el ecg de prueba.
 */
void Timer2_Interrupt(void);

/** @brief Lee datos que se envian por la UART.
 * Si es la letra "P" activa/desactiva el respirador
 *
 */
void Activar_Sistema(void);

/*==================[external data definition]===============================*/

//timer_config timer={TIMER_A ,2,Timer_Interrupt};/**< Configuracion del timer para conversion analogica digital */
timer_config timer2={TIMER_B ,17,Timer2_Interrupt};/**< Configuracion del timer para salida por dac. */
analog_input_config analog_input={CH1,AINPUTS_SINGLE_READ,Conversion_AD};/**< Configuracion del conversor AD */
serial_config  configuracion_uart={SERIAL_PORT_PC,115200,&Activar_Sistema};/**< Configuracion de la uart */

/*==================[external functions definition]==========================*/
void Sist_Init(void)
{
	SystemClockInit();
	//TimerInit(&timer);
	AnalogInputInit(&analog_input);
	UartInit(&configuracion_uart);
	AnalogOutputInit();
	TimerInit(&timer2);
	TimerStart(TIMER_B);
	motor_config configuracion={2050,15,30};
	MotorInit(GPIO_1,GPIO_3,GPIO_5,GPIO_T_FIL0,&configuracion);

	if (play==ACTIVADO){
		//TimerStart(TIMER_A);
	}

}

void Timer_Interrupt(void)
{
	AnalogStartConvertion();
}
void Conversion_AD(void)
{   dato_leido_anterior=dato_leido;
	AnalogInputRead(CH1, &dato_leido);


UartSendString(SERIAL_PORT_PC, UartItoa(dato_leido, 10));
UartSendString(SERIAL_PORT_PC,"\r");
if (termino==true){
	if (dato_leido_anterior>CERO_CORRIDO&dato_leido<CERO_CORRIDO){
		aux=1;
		termino=false;

		}
}

}
void Timer2_Interrupt(void)
{
	AnalogOutputWrite(presion[contador]);
	contador++;
	if (contador==BUFFER_SIZE)
			{
			contador=0;
	}

	if (play==ACTIVADO){
	Timer_Interrupt();}
}
void Activar_Sistema(void)
{

uint8_t dato;
UartReadByte(SERIAL_PORT_PC, &dato);
if (dato=='P'){
	play=!play;
}

if (play==ACTIVADO){
	//TimerStart(TIMER_A);
}
if (play==DESACTIVADO){
	//TimerStop(TIMER_A);
}


}





/** @fn int main(void)
*	@brief funcion principal.
*
*	@return 0
*/
int main(void)
{
    Sist_Init();

	while(1){
		if(aux==1){
			MotorStart(1);
			termino=MotorStart(0);
			aux=0;

		}

	}
	return 0;
}

/*==================[end of file]============================================*/

