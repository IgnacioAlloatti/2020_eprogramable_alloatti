/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * Ignacio Alloatti

 */
/** @mainpage Medidor de distancia con envio de datos mediante UART-USB
*
* @section genDesc Descripcion general
* Mide la distancia mediante sensor hc_sr4 y envia datos para mostrar por UART-USB
*
*<a href="https://drive.google.com/file/d/1EnEo6xdEKuuoyUowY9cm_vqtsPCliFA-/view?usp=sharing">Operation Example</a>
*
*
* |   Device 1	   |   EDU-CIAA	    |
* |:--------------:|:--------------:|
* | 	Echo	   | 	GPIO_T_FIL2 |
* | 	Trigger	   | 	GPIO_T_FIL3	|
*
*@section changelog Cambios
*
* |   Date	   | Description                                    |
* |:----------:|:-----------------------------------------------|
* | 25/09/2020 | Creacion del documento	                        |
*
* @author Ignacio Alloatti
*
*/

/*==================[inclusions]=============================================*/
#include "../inc/hc_sr4_con_timer.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "switch.h"
#include "delay.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
/** @def DISTANCIA_10
*	@brief distancia de 10 cm
*/
#define DISTANCIA_10 10
/** @def DISTANCIA_20
*	@brief distancia de 20 cm
*/
#define DISTANCIA_20 20
/** @def DISTANCIA_30
*	@brief distancia de 30 cm
*/
#define DISTANCIA_30 30
/** @def ACTIVADA
*	@brief Funcion del proyecto activada
*/
#define ACTIVADA 1
/** @def DESACTIVADA
*	@brief Funcion del proyecto desactivada
*/
#define DESACTIVADA 0

uint16_t result;/**< Variable auxiliar para guardar la distancia leida en cm */
uint16_t medicion=DESACTIVADA;/**< Variable auxiliar para guardar estado del sensor */
uint16_t hold=DESACTIVADA;/**< Variable auxiliar para guardar estado de funcion "mantener medicion" */





/*==================[internal functions declaration]=========================*/

/** @brief Activa/Desactiva la medicion del sensor Hc_sr4, por interrupcion al apretar la tecla 1
 */
void Activar_Medicion(void);

/** @brief Activa/Desactiva la funcion "mantener medicion", por interrupcion al apretar la tecla 2
 */
void Hold(void);

/** @brief Actualiza la medicion y la guarda en la variable result. Envia el dato por la UART
 */
void ActualizarMuestra(void);

/** @brief Lee datos que se envian por la UART.
 * Si es la letra "h" activa/desactiva la funcion "mantener medicion"
 *Si es la letra "o"  activa/desactiva la medicion
 */
void Recepcion_Uart(void);

/** @brief Inicializacion de los distintos perifericos a usar
 */
void Sistem_Inicialize(void);


/*==================[external data definition]===============================*/
timer_config timer_init={TIMER_A,1000,ActualizarMuestra};/**< Configuracion del timer */
serial_config configuracion_uart={SERIAL_PORT_PC,115200,&Recepcion_Uart};/**< Configuracion de la UART */
/*==================[external functions definition]==========================*/

void ActualizarMuestra(void)
{ if (hold==DESACTIVADA){
						result  = HcSr04ReadDistanceCentimeters();
                        }
	UartSendString(SERIAL_PORT_PC,UartItoa(result, 10));
	UartSendString(SERIAL_PORT_PC," cm\r\n");

}
void Recepcion_Uart(void){
	uint8_t dato;
	UartReadByte(SERIAL_PORT_PC, &dato);
	if (dato=='O'){
		Activar_Medicion();
	}
	if(dato=='H'){
		Hold();
	}

}
void Sistem_Inicialize(void)
{
	SystemClockInit();
	SwitchesInit();
	LedsInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	SwitchActivInt(SWITCH_1,&Activar_Medicion);
	SwitchActivInt(SWITCH_2,&Hold);
	TimerInit(&timer_init);
	UartInit(&configuracion_uart);
}

void Activar_Medicion(void)
{
	medicion = !medicion;
	if(medicion==ACTIVADA){
		TimerStart(TIMER_A);}
	else {TimerStop(TIMER_A);
	hold=DESACTIVADA;
	}

}

void Hold(void)
{   if (medicion==ACTIVADA){
	hold = !hold;
			}
}
/** @fn int main(void)
*	@brief funcion principal.
*
*	@return 0
*/
int main(void)
{
	Sistem_Inicialize();

	while(1){


             }

    
	return 0;
}

/*==================[end of file]============================================*/

