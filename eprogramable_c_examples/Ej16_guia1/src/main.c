/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/
#define BIT8 8
#define BIT16 16
#define BIT24 24

typedef union{
	 struct{
		uint8_t byte1;
		uint8_t byte2;
		uint8_t byte3;
		uint8_t byte4;
	}bytes;
	uint32_t  todos_los_bytes;
}test;


/*==================[internal functions declaration]=========================*/


int main(void)
{
   uint32_t a=0x01020304;
   uint8_t byte1 , byte2, byte3 , byte4;
   uint32_t mask= 0x000000FF ;
   printf("el valor de la mascara es: %d \r\n", mask);
   byte1=(uint8_t) a & mask; //truncamiento (igualo variable de 32 bit a una de 8 bit), casteo es necesario si usamos punteros
   printf("el valor de byte1 es: %d \r\n", byte1);
   byte2= (a>>BIT8) & mask;
   printf("el valor de byte2 es: %d \r\n", byte2);
   byte3= (a>>BIT16)& mask;
   printf("el valor de byte3 es: %d \r\n", byte3);
   byte4= (a>>BIT24)& mask;
   printf("el valor de byte4 es: %d \r\n", byte4);

test prueba;
prueba.todos_los_bytes=a;
printf("el valor de byte1 es: %d \r\n",prueba.bytes.byte1);
printf("el valor de byte2 es: %d \r\n",prueba.bytes.byte2);
printf("el valor de byte3 es: %d \r\n",prueba.bytes.byte3);
printf("el valor de byte4 es: %d \r\n",prueba.bytes.byte4);







	return 0;
}

/*==================[end of file]============================================*/

