/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/
#define ON 1
#define OFF 0
#define TOGGLE 2
#define LED1 1
#define LED2 2
#define LED3 3
struct leds
{
	uint8_t n_led;      //indica el número de led a controlar
	uint8_t n_ciclos;   //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    //indica el tiempo de cada ciclo
	uint8_t mode;       //ON, OFF, TOGGLE
} ;
void ControlLed(struct leds *myLed);

/*==================[internal functions declaration]=========================*/
void ControlLed(struct leds *myLed)
{if (myLed->mode==ON){
	                switch (myLed->n_led){
	                					case LED1: printf("Led %d encendido \r\n",myLed->n_led);
	                					break;
	                					case LED2: printf("Led 2 encendido \r\n");
	                					break;
	                					case LED3: printf("Led 3 encendido \r\n");
	                					break;
	                                     }
                     }
if (myLed->mode==OFF){
	switch (myLed->n_led){
		                case LED1: printf("Led 1 apagado \r\n");
		                break;
		                case LED2: printf("Led 2 apagado \r\n");
		                break;
		                case LED3: printf("Led 3 apagado \r\n");
		                break;
		                 }


					  }
if (myLed->mode==TOGGLE){
						uint8_t i=0;
						uint8_t j;
						for (i=0; i<myLed->n_ciclos;i++){
													switch (myLed->n_led){
								                					case LED1: printf("Led 1 toggle \r\n");
								                					break;
								                					case LED2: printf("Led 2 toggle \r\n");
								                					break;
								                					case LED3: printf("Led 3 toggle \r\n");
								                					break;
								                                     }
														j=0;
														while(j<myLed->periodo){
																				j++;
																				}								     						}



						}
}



int main(void)
{
	struct leds mi_led = {LED1, 5, 300 ,ON};
	ControlLed(&mi_led);
	return 0;


}

/*==================[end of file]============================================*/

