########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
PROYECTO_ACTIVO = 1_hola_mundo
NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = Ej1_guia1
#NOMBRE_EJECUTABLE = ej1.exe

#PROYECTO_ACTIVO = Ej2_guia1
#NOMBRE_EJECUTABLE = ej2.exe

#PROYECTO_ACTIVO = Ej7_guia1
#NOMBRE_EJECUTABLE = ej7.exe

#PROYECTO_ACTIVO = Ej9_guia1
#NOMBRE_EJECUTABLE = ej9.exe

#PROYECTO_ACTIVO = Ej12_guia1
#NOMBRE_EJECUTABLE = ej12.exe

#PROYECTO_ACTIVO = Ej13_guia1
#NOMBRE_EJECUTABLE = ej13.exe

#PROYECTO_ACTIVO = Ej16_guia1
#NOMBRE_EJECUTABLE = ej16.exe

PROYECTO_ACTIVO = Ej17_guia1
NOMBRE_EJECUTABLE = ej17.exe